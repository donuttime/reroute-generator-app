import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Provider} from 'react-redux';

import store from './src/redux-state/store';
import MaxDeltaInput from './src/app-ui/MaxDeltaInput';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
        <View style={{
            height : 100
        }}/>
        <MaxDeltaInput/>
          <Text>Open up App.js to start working on your app!</Text>
          <Text>Changes you make will automatically reload.</Text>
          <Text>Shake your  phone to open the developer menu.</Text>
        </View>
      </Provider>
    );
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection:"column",
  },
});
