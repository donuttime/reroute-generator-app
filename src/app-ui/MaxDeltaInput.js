import React, {Component} from 'react';
import { StyleSheet, Text, View, Picker, TouchableWithoutFeedback } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

const timeOptions = [
    '10 minutes',
    '15 minutes',
    '20 minutes',
    '30 minutes',
];

class MaxDeltaInputContainer extends Component {

    constructor(props) {
        super(props);
        this._extractTimeElement = this._extractTimeElement.bind(this);
        this._extractTime = this._extractTime.bind(this);
    }

    _extractTime(statement) {
        return statement
            .split(' ')
            .reduce(function(res, val, ind, arr) {
                if(ind % 2 === 0) {
                    res.push([arr[ind], arr[ind+1]]);
                }
                return res;
            }, [])
            .map(this._extractTimeElement)
            .reduce((res, val) => res + val, 0);

    }

    _extractTimeElement(parts) {
        var num = parts[0];
        var unit = parts[1];
        
        if(!Number.isInteger(num)) {
            num = Number.parseInt(num, 10);
        }

        var factor = 1;
        if(!unit) {
            return num;
        }
        else if(unit.toLowerCase().indexOf('m') !== -1) {
            factor = 60;
        }
        else if(unit.toLowerCase().indexOf('h') !== -1) {
            factor = 3600;
        }

        return num * factor;
    }

    _encodeTime(seconds) {
        if(!Number.isInteger(seconds)) {
            seconds = Number.parseInt(seconds, 10);
        }
        let idx = timeOptions.map(this._extractTime).findIndex(val => val === seconds);
        if(idx > -1) {
            return [timeOptions[idx], idx];
        }
        else return [this._encodeTimeDirect(seconds), -1];
    }

    _encodeTimeDirect(seconds, compound = false) {
        seconds = Number.isInteger(seconds) ? seconds : Number.parseInt(seconds, 10);

        if(!compound || Math.abs(seconds) < 3600) {
            return (seconds/60) + ' minutes';
        }

        else {
            let hours = seconds/3600;
            let minutes = (seconds /60) % 60;
            return hours + ' hours ' + minutes + ' minutes';
        }
    }

    render () {
        return (
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'space-between',
                borderBottomWidth : 1,
                borderTopWidth : 1,
                borderRightWidth : 1,
                borderLeftWidth : 1,
                padding : 1,
                height: 30,
                backgroundColor : 'white',
                borderColor : 'orange',
            }}>
                <Text style={{ 
                    flex: 1,
                    backgroundColor : "white",
                    textAlign : 'left',
                    fontSize: 18,
                    }}>Max Time:</Text>
                <View style = {{
                    flexGrow: 1
                }}/>
                <ModalDropdown
                    ref = {(dropdown) => this.dropdown = dropdown}
                    options = {timeOptions}
                    animated = {false}
                    defaultIndex = {this._encodeTime(this.props.value)[1]}
                    defaultValue = {this._encodeTime(this.props.value)[0]}
                    onSelect = {(idx, value) => this.props.onClick(this._extractTime(value))}
                    textStyle = {{
                        fontSize : 18,
                        textAlign: 'right',
                    }}
                    dropdownTextStyle = {{
                        fontSize : 18,
                        textAlign: 'right',
                        color : 'black',
                    }}
                    dropdownTextHighlightStyle = {{
                        fontSize : 18,
                        textAlign: 'right',
                        color : 'grey',
                    }}
                    style = {{
                        flex : 1,
                        backgroundColor : 'white',
                    }}
                    />
            </View>
        );
    }
}


import {setMaxDelta} from '../redux-state/actions/editQuery';
import { connect } from 'react-redux';

function mapStateToProps(state) {
    return {
        value : state.queryparams.maxDelta
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onClick : (stringval) => {
            
            dispatch(setMaxDelta(Number.parseInt(stringval, 10)));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MaxDeltaInputContainer);