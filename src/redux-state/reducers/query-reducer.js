import {
    SET_START_TEXT_REQUEST, SET_START_TEXT_SUCCESS, SET_START_TEXT_ERROR,
    SET_START_COORDS_REQUEST, SET_START_COORDS_SUCCESS, SET_START_COORDS_ERROR,
    SET_MAX_DELTA, SET_START_TIME, SET_QUERY_STRING

} from '../actions/actiontypes';


/* Start Location is complicated so we give it its own reducer then nest */

const initialStartLocation = {
    latitude : -999,
    longitude : -999,
    name : "",
    geocoding : false,
    revgeocoding : false,
    errors : []
};

const startReducer = (state = initialStartLocation, action) => {
    switch(action.type) {
        
        case SET_START_COORDS_REQUEST : {
            return {
                latitude : action.payload.latitude,
                longitude : action.payload.longitude,
                name : "",
                geocoding : true,
                revgeocoding : false,
                errors : []
            };
        }

        case SET_START_COORDS_SUCCESS : {
            return {
                ...state, 
                name : action.payload.name,
                geocoding : false,
                errors : []
            };
        }

        case SET_START_COORDS_ERROR : {
            return {
                ...state,
                name : "Unknown Address",
                geocoding : false,
                errors: [...state.errors, action.payload]
            };
        }

        case SET_START_TEXT_REQUEST : {
            return {
                latitude : -999,
                longitude : -999,
                name : action.payload.name,
                geocoding : false,
                revgeocoding : true,
                errors : []
            };
        }

        case SET_START_TEXT_SUCCESS : {
            return {
                ...state, 
                latitude : action.payload.latitude,
                longitude : action.payload.longitude,
                revgeocoding : false,
                errors : []
            };
        }

        case SET_START_TEXT_ERROR : {
            return {
                ...state,
                latitude : -999,
                longitude : -999,
                revgeocoding : false,
                errors: [...state.errors, action.payload]
            };
        }

        default: {
            return state;
        }
        
    }
}


/* This is the central query reducer that we export */

const initialState = {
    startTime : -1,
    maxDelta : 15 * 60,
    queryString : '',
    startLocation : initialStartLocation
}

const queryReducer = (state = initialState, action) => {
    switch(action.type) {

        case SET_MAX_DELTA : {
            return {
                ...state,
                maxDelta : action.payload.maxDelta
            }
        }

        case SET_START_TIME : {
            return {
                ...state,
                startTime : action.payload.startTime
            }
        }

        case SET_QUERY_STRING : {
            return {
                ...state,
                queryString : action.payload.queryString
            }
        }

        default : {
            return {
                ...state,
                startLocation : startReducer(state.startLocation, action)
            }
        }
    }
}

export default queryReducer;