import {combineReducers} from 'redux';

import queryparams from './query-reducer';
import routedata from './data-reducer';

export default combineReducers({queryparams, routedata});