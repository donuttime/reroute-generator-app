import {
    RUN_QUERY_REQUEST, RUN_QUERY_SUCCESS, RUN_QUERY_ERROR
} from '../actions/actiontypes';

const initialState = {
    data : {},
    errors : [],
    runningQuery : false,
    lastParams : {}
}

const dataReducer = (state = initialState, action) => {
    switch(action.type) {

        case RUN_QUERY_REQUEST : {
            return {
                ...state,
                data : {},
                runningQuery : true,
                lastParams : action.payload.queryParams
            };
        }

        case RUN_QUERY_SUCCESS : {
            return {
                ...state,
                data : action.payload.data,
                runningQuery : false,
                errors : []
            };
        }

        case RUN_QUERY_ERROR : {
            return {
                ...state,
                runningQuery : false,
                errors : [...state.errors, action.payload]
            };
        }

        default : {
            return state;
        }
    }
}

export default dataReducer;