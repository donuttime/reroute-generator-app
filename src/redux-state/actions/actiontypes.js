
/* Query Running */
export const RUN_QUERY_SUCCESS = 'RUN_QUERY_SUCCESS';
export const RUN_QUERY_ERROR = 'RUN_QUERY_ERROR';
export const RUN_QUERY_REQUEST = 'RUN_QUERY_REQUEST';

/* Start Location Changing via Text */
export const SET_START_TEXT_REQUEST = 'SET_START_TEXT_REQUEST';
export const SET_START_TEXT_SUCCESS = 'SET_START_TEXT_SUCCESS';
export const SET_START_TEXT_ERROR = 'SET_START_TEXT_ERROR';

/* Start Location Changing via Current Location */
export const SET_START_COORDS_REQUEST = 'SET_START_COORDS_REQUEST';
export const SET_START_COORDS_ERROR = 'SET_START_COORDS_ERROR';
export const SET_START_COORDS_SUCCESS = 'SET_START_COORDS_SUCCESS';

/* Start Time Changing */
export const SET_START_TIME = 'SET_START_TIME';

/* Max Delta Changing */
export const SET_MAX_DELTA = 'SET_MAX_DELTA';

/* Query String Changing */
export const SET_QUERY_STRING = 'SET_QUERY_STRING';