import {
    SET_MAX_DELTA, SET_QUERY_STRING, SET_START_TIME,
    SET_START_COORDS_REQUEST, SET_START_COORDS_SUCCESS, SET_START_COORDS_ERROR,
    SET_START_TEXT_REQUEST, SET_START_TEXT_SUCCESS, SET_START_TEXT_ERROR,
} from './actiontypes';

import {geocode, reverseGeocode} from '../../services/geocoder';


/* The easy parameters */
export const setMaxDelta = (maxDelta) => ({
    type : SET_MAX_DELTA, 
    payload : {
        maxDelta : maxDelta
    }
});

export const setQueryString = (queryString) => ({
    type : SET_QUERY_STRING, 
    payload : {
        queryString : queryString
    }
});

export const setStartTime = (startTime) => ({
    type : SET_START_TIME,
    payload : {
        startTime : startTime
    }
});

/* Setting start location via text */

const setStartTextRequest = (startText) => ({
    type : SET_START_TEXT_REQUEST,
    payload :{
        name : startText
    }
});

const setStartTextSuccess = (coords) => ({
    type : SET_START_TEXT_SUCCESS,
    payload :{
        latitude : coords.latitude,
        longitude : coords.longitude
    }
});

const setStartTextError = (err) => ({
    type : SET_START_TEXT_ERROR,
    payload : err
})

export const setStartText = (startText) => (dispatch) => {
    dispatch(setStartTextRequest(startText));
    geocode(startText)
        .then((coords) => dispatch(setStartTextSuccess(coords)))
        .catch((err) => dispatch(setStartTextError(err)));
}

/* Setting the start location via coords */

const setStartCoordsRequest = (startCoords) => ({
    type : SET_START_COORDS_REQUEST,
    payload :{
        latitude : startCoords.latitude,
        longitude : startCoords.longitude,
    }
});

const setStartCoordsSuccess = (name) => ({
    type : SET_START_COORDS_SUCCESS,
    payload :{
        name : name
    }
});

const setStartCoordsError = (err) => ({
    type : SET_START_COORDS_ERROR,
    payload : err
})

export const setStartCoords = (startCoords) => (dispatch) => {
    dispatch(setStartCoordsRequest(startCoords));
    reverseGeocode(startCoords)
        .then((coords) => dispatch(setStartCoordsSuccess(coords)))
        .catch((err) => dispatch(setStartCoordsError(err)));
}