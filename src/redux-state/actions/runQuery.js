import {RUN_QUERY_ERROR, RUN_QUERY_SUCCESS, RUN_QUERY_REQUEST} from './actiontypes';
import {callQuery} from '../../services/queryrunner';

const runQueryError = (errormsg) => ({
    type : RUN_QUERY_ERROR,
    payload: {
        message : errormsg
    }
});

const runQuerySuccess = (data) => ({
    type : RUN_QUERY_SUCCESS,
    payload : {
        data : data
    }
});

const runQueryRequest = (queryParams) => ({
    type : RUN_QUERY_REQUEST,
    payload : {
        queryParams : queryParams
    }
})

export const runQuery = (queryParams) => (dispatch) => {
    dispatch(runQueryRequest(queryParams));
    callQuery(queryParams)
        .then((res) => dispatch(runQuerySuccess(res)))
        .catch((err) => runQueryError(JSON.stringify(err)))
}